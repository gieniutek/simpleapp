package com.example.uytkownik.simpleapp.Adapter;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.uytkownik.simpleapp.Model.Post;
import com.example.uytkownik.simpleapp.R;

import java.util.Date;
import java.util.List;

/**
 * Created by Użytkownik on 10.12.2017.
 */

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.PostViewHolder> {

    public interface OnItemClickListener {
        void onItemClick(Post item);
    }


    private List<Post> postList;
    private final OnItemClickListener listener;

    public class PostViewHolder extends RecyclerView.ViewHolder {
        public TextView postName;
        public TextView author;
        public ImageView image;
        public TextView date;
        public TextView descr;
        private Context context;
        private View view;

        public PostViewHolder(View view) {
            super(view);
            //TODO: Bind view with post_item
            context = view.getContext();
            postName = view.findViewById(R.id.tvPostName);
            date = view.findViewById(R.id.tvDate);
            descr = view.findViewById(R.id.tvDesc);
            author = view.findViewById(R.id.tvAuthor);
            image = view.findViewById(R.id.ivImage);
            this.view = view;
        }

        public void bindListener(final Post item, final OnItemClickListener listener) {
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(item);
                }
            });
        }
    }


    public PostAdapter(List<Post> postList, OnItemClickListener listener) {
        this.postList = postList;
        this.listener = listener;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public PostAdapter.PostViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.post_item, parent, false);

        return new PostViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PostViewHolder holder, int position) {
        Log.d("onBindViewHolder:","building post!");
        Post post = postList.get(position);
        Date date = new Date(post.getTimeStamp());
        holder.author.setText(post.getAuthor().toString());
        holder.postName.setText(post.getTitle());
        holder.date.setText(date.toString());
        holder.descr.setText(post.getDesc());
        holder.bindListener(post,listener);

        Glide.with(holder.context).load(post.getImageUrl()).into(holder.image);
    }

    @Override
    public int getItemCount() {
        return postList.size();
    }

}

