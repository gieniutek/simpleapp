package com.example.uytkownik.simpleapp.Model;

import com.example.uytkownik.simpleapp.Model.Author;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Użytkownik on 11.12.2017.
 */

public class PostDedails {

    @SerializedName("id")
    private long ID;
    @SerializedName("timestamp")
    private long timeStamp;
    @SerializedName("title")
    private String title;
    @SerializedName("author")
    private Author author;
    @SerializedName("image")
    private String imageUrl;
    @SerializedName("image_big")
    private String imageBigUrl;
    @SerializedName("description")
    private String desc;

    public PostDedails(long ID, long timeStamp, String title, Author author, String imageUrl, String imageBigUrl, String desc) {
        this.ID = ID;
        this.timeStamp = timeStamp;
        this.title = title;
        this.author = author;
        this.imageUrl = imageUrl;
        this.imageBigUrl = imageBigUrl;
        this.desc = desc;
    }

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageBigUrl() {
        return imageBigUrl;
    }

    public void setImageBigUrl(String imageBigUrl) {
        this.imageBigUrl = imageBigUrl;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
