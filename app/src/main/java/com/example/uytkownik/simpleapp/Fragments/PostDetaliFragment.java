package com.example.uytkownik.simpleapp.Fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.uytkownik.simpleapp.Model.PostDedails;
import com.example.uytkownik.simpleapp.ApiService.PostService;
import com.example.uytkownik.simpleapp.R;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.ExecutionException;

import retrofit2.Call;

public class PostDetaliFragment extends Fragment {

    private ImageView imageView;
    private TextView tvTitle;
    private TextView tvAuthor;
    private TextView tvDate;
    private TextView tvDescr;
    private PostDedails post;
    private long id;
    private View view;

    public PostDetaliFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        id=getArguments().getLong("id");
        view = inflater.inflate(R.layout.fragment_post_detali, container, false);
        initView();
        return view;
    }

    void initView(){
        imageView = view.findViewById(R.id.imageView2);
        tvTitle = view.findViewById(R.id.tvDTitle);
        tvAuthor = view.findViewById(R.id.tvDAuthor);
        tvDate = view.findViewById(R.id.tvDData);
        tvDescr = view.findViewById(R.id.tvDDescr);
        try {
            post = new GetPost().execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        Glide.with(getContext()).load(post.getImageUrl()).into(imageView);
        Date date = new Date(post.getTimeStamp());
        tvTitle.setText(post.getTitle());
        tvAuthor.setText(post.getAuthor().toString());
        tvDate.setText(date.toString());
        tvDescr.setText(post.getDesc());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private class GetPost extends AsyncTask<String, Integer, PostDedails> {
        PostDedails result;

        public GetPost() {
        }

        protected PostDedails doInBackground(String... urls) {

            PostService postService = PostService.retrofit.create(PostService.class);
            Call<PostDedails> call = postService.getPost(id);
            try {
                result = call.execute().body();
                Log.d("Debug: ","Executed in background "+result.getDesc());
            } catch (IOException e) {
                Log.d("Debug: ","U're suck");
                e.printStackTrace();
            }

            return result;
        }
        protected void onProgressUpdate(Integer... id) {

        }

        protected void onPostExecute(PostDedails result) {
            post = result;
        }
    }
}

