package com.example.uytkownik.simpleapp.Activity;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.example.uytkownik.simpleapp.Fragments.PostListFragment;
import com.example.uytkownik.simpleapp.R;

public class MainActivity extends FragmentActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (findViewById(R.id.fragment_container) != null) {
             if (savedInstanceState != null) {
                return;
            }
            PostListFragment firstFragment = new PostListFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, firstFragment).commit();
        }
    }
}
