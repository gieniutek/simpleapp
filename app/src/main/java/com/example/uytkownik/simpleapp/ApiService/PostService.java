package com.example.uytkownik.simpleapp.ApiService;

import com.example.uytkownik.simpleapp.Model.Post;
import com.example.uytkownik.simpleapp.Model.PostDedails;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Użytkownik on 10.12.2017.
 */

public interface PostService {

    @GET("posts")
    Call<List<Post>> repoContributors();

    @GET("post/{post_id}")
    Call<PostDedails> getPost(@Path("post_id") long id);

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://apps.nd0.pl/mock/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();
}
