package com.example.uytkownik.simpleapp.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Użytkownik on 11.12.2017.
 */

public class Author {

        @SerializedName("firstName")
        String firstName;
        @SerializedName("lastName")
        String lastName;
        @SerializedName("nick")
        String nick;

        public Author(String firstName, String lastName, String nick) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.nick = nick;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getNick() {
            return nick;
        }

        public void setNick(String nick) {
            this.nick = nick;
        }

        @Override
        public String toString() {
            return firstName + " " + lastName+ " " + "\"" + nick + "\"";
        }
    }

