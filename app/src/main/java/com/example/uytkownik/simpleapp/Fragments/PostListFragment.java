package com.example.uytkownik.simpleapp.Fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.uytkownik.simpleapp.Model.Post;
import com.example.uytkownik.simpleapp.Adapter.PostAdapter;
import com.example.uytkownik.simpleapp.ApiService.PostService;
import com.example.uytkownik.simpleapp.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

public class PostListFragment extends Fragment{

    private List<Post> postList = new ArrayList<>();
    private RecyclerView recyclerView;
    private PostAdapter mAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RelativeLayout emptyView;
    private View view;

    public PostListFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_posts_list, container, false);
        initView();
        setupListeners();
        return view;
    }

    void initView(){
        emptyView = view.findViewById(R.id.emptyView);
        mSwipeRefreshLayout = view.findViewById(R.id.swipePostListView);
        recyclerView = view.findViewById(R.id.recyclerView);
        mAdapter = new PostAdapter(postList, new PostAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Post item) {
                PostDetaliFragment newFragment = new PostDetaliFragment();
                Bundle args = new Bundle();
                args.putLong("id", item.getID());
                newFragment.setArguments(args);

                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container,newFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        enableOrDisableVisibilityRV();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(mAdapter);

    }
    void setupListeners(){
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new RefreshPost().execute();
                Toast.makeText(getContext(),"Refreshed",Toast.LENGTH_LONG).show();

            }
        });
    }

    void enableOrDisableVisibilityRV(){
        if(postList.size()!=0) {
            emptyView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }else{
            emptyView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.INVISIBLE);
        }
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private class RefreshPost extends AsyncTask<String, Integer, String> {
        List<Post> result;

        public RefreshPost() {
        }

        protected String doInBackground(String... urls) {

            PostService postService = PostService.retrofit.create(PostService.class);
            Call<List<Post>> call = postService.repoContributors();
            try {
                result = call.execute().body();
            } catch (IOException e) {
                e.printStackTrace();
            }
            for (Post post:result ) {
                if(-1==postList.indexOf(post)) {
                    postList.add(post);
                    publishProgress(1);
                }
            }
            return "";
        }
        protected void onProgressUpdate(Integer... id) {
            mAdapter.notifyDataSetChanged();
        }

        protected void onPostExecute(String result) {
            mAdapter.notifyDataSetChanged();
            enableOrDisableVisibilityRV();
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }
}

