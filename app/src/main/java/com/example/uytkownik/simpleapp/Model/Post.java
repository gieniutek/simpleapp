package com.example.uytkownik.simpleapp.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Użytkownik on 10.12.2017.
 */

public class Post {

    @SerializedName("id")
    private long ID;
    @SerializedName("timestamp")
    private long timeStamp;
    @SerializedName("title")
    private String title;
    @SerializedName("author")
    private Author author;
    @SerializedName("image")
    private String imageUrl;
    @SerializedName("short_description")
    private String desc;

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long dateCreated) {
        this.timeStamp = dateCreated;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String url) {
        this.imageUrl = url;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String body) {
        this.desc = body;
    }

    public Post(long ID, long timeStamp, String title, Author author, String imageUrl, String desc) {
        this.ID = ID;
        this.timeStamp = timeStamp;
        this.title = title;
        this.author = author;
        this.imageUrl = imageUrl;
        this.desc = desc;
    }

    @Override
    public String toString() {
        return "Post{" +
                "ID=" + ID +
                ", timeStamp=" + timeStamp +
                ", title='" + title + '\'' +
                ", author=" + author +
                ", imageUrl='" + imageUrl + '\'' +
                ", desc='" + desc + '\'' +
                '}';
    }

}
